function addTokens(input, tokens){
    
    if (typeof input !== 'string') {
        throw new Error('Input should be a string');
    }    
    if (input.length < 6) {
        throw new Error('Input should have at least 6 characters');
    }

    let valid = true;
    
    tokens.forEach((element) => {
        
        if (typeof element !== 'object') {
            valid = false;
        } else {
            let cheie = Object.keys(element);
            let valoare = Object.values(element);
           
            cheie.forEach((key) => {
                if (key !== 'tokenName') {
                    valid = false;
                }
            });
           
            if (valid) {
                valoare.forEach((value) => {
                    if (typeof value !== 'string') {
                        valid = false;
                    }
                });              
            }
        }
    });
    if (!valid) {
        throw new Error('Invalid array format');
    }
    

    
    if (input.indexOf('...') == -1) {
        return input;
    }

    tokens.map((e) => {
        input = input.replace('...', '${' + e.tokenName + "}");
    });
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;